import { Injectable } from '@angular/core';
import { HttpClient }from '@angular/common/http'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SrvService {
  apiKey = 'AIzaSyDFkD_pDDWItzWrTsfW-Mi68Ho5UbPCM2o'

  constructor(  public http: HttpClient) { }

  getPlaylist(ch_id){
    return this.http.get('https://www.googleapis.com/youtube/v3/playlist?key='+ this.apiKey + '&channelId' + ch_id + '&part=snippet,id&maxResults=20').pipe(map(res => {return res['items'];}));
    
  }
  getList(ls_id){
    return this.http.get('https://www.googleapis.com/youtube/v3/playlistItems?key='+ this.apiKey + '&PlaylistId' +ls_id + '&part=snippet,id&maxResults=20').pipe(map(res => {return res['items'];}));

  }
}

