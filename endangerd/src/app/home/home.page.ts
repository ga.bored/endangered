import { Component } from '@angular/core';
import { SrvService } from '../srv.service';
import { NavController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  channelId= 'UC-lHJZR3Gqxm24_Vd_AJ5Yw';
  playlist: Observable<any[]>
  constructor( private srv: SrvService, public router: Router, public alrt: AlertController) {}
  async presentAlert() {
    const alert = await this.alrt.create({
      header: 'Error',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }
  searchPlaylist(){
    this.playlist = this.srv.getPlaylist(this.channelId);
    this.playlist.subscribe(data =>{
      console.log('data: ', data);
    }, err =>{
        this.presentAlert();

    });

  }
  openPlaylist(id){
    this.router.navigate(['data', {id: id}]);
  }

}
